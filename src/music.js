/*
    @official music library of SchwiftyBot
    @author Om Bhende
    @date 8 April 2020 (I hope corona goes away)
    @project SchwiftyBot
*/

const ytdl = require("ytdl-core");
const youtube = require("youtube-info");
const config = require("./Config.js");

var queue = []; // queue[serverid] - will return a table with possible queue from server
var conn = []
var channel = [] // Current channel that has playing music.
var timer = [] // Timer to leave voiceChannel
var channelCmdUsed = [] // text channel to save

function toTime(time) {
    var hrs = ~~(time / 3600);
    var mins = ~~((time % 3600) / 60);
    var secs = ~~time % 60;
    var ret = "";
    if (hrs > 0) {
        ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
    }
    ret += "" + mins + ":" + (secs < 10 ? "0" : "");
    ret += "" + secs;
    return ret;
}

module.exports.addQueue = function(guild, songURL, msg, voiceChan) {
    if (songURL == null || songURL == undefined || !songURL) return msg.reply("Oops! Something went wrong. Try using the command again.");
    channelCmdUsed[guild.id] = msg;
    if (channel[guild.id] == undefined || !channel[guild.id]) {
        channel[guild.id] = voiceChan;
    }
    if (voiceChan != channel[guild.id]) return msg.reply("You need to be in the same voice channel to use this command (#" + channel[guild.id].name + ")");
    if (!this.getVideoId(songURL)) return false;
    channel[guild.id] = voiceChan;
    if (queue[guild.id] == undefined || !queue[guild.id] || queue[guild.id].length == 0 || !songURL) { 
        queue[guild.id] = [];
        var stream1 = this.stream("nil", msg, songURL);
        if (stream1) {
        youtube(this.getVideoId(songURL), function (err, videoInfo) {
            if (err) return console.log(err);
            msg.channel.send(":notes:  **Now Playing:** ``" + videoInfo.title + "`` **Uploaded by**: ``" + videoInfo.owner + "`` **Duration: ** ``" + toTime(videoInfo.duration) + "``");
            queue[guild.id].push([songURL, videoInfo.title]);
          });
        }
    } else {
        if (queue[guild.id].length == 0) {
            this.stream("nil", msg, songURL);
        }
        youtube(this.getVideoId(songURL), function (err, videoInfo) {
            if (err) return console.log(err);
            queue[guild.id].push([songURL, videoInfo.title])
            msg.channel.send(":notes:  **Added to Queue:** ``" + videoInfo.title + "`` **Uploaded by**: ``" + videoInfo.owner + "`` **Duration: ** ``" + toTime(videoInfo.duration) + "``");
        });
    }
}

module.exports.nextSong = function(server) {
    if (!server || server == "") {
        return false;
    } else {
        if (queue[server] == undefined || queue[server].length == 0) { return false; }
        queue[server].shift();
        if (!queue[server][0] || queue[server][0] == undefined || queue[server][0] == false) { return false; }
        return queue[server][0][0]  || false;
    }
}

module.exports.leaveChannel = function(msg) {
    if (!msg.guild.id || msg.guild.id == undefined) {
        return false;
    } else {
        queue[msg.guild.id] = [];
        channel[msg.guild.id].leave();
        if (timer[msg.guild.id]) clearTimeout(timer[msg.guild.id]);
        return msg.channel.send(":x: I have disconnected to the channel!");
    }
}

module.exports.getNextSongAfterSkip = function(server) {
    if (!server || server == "") {
        return false;
    } else {
        if (queue[server] == undefined || queue[server].length == 0) { return false; }
        if (!queue[server][0] || queue[server][0] == undefined || queue[server][0] == false) { return false; }
        return queue[server][0][0]  || false;
    }
}

module.exports.skipTrack = function(msg, voiceChan) {
    if (channel[msg.guild.id] == undefined) return false;
    if (voiceChan != channel[msg.guild.id]) return msg.reply("You need to be in the same voice channel to use this command (#" + channel[guild.id].name + ")");
    this.stream("skip", msg, "https://www.youtube.com/watch?v=qFmCXBL_4n8") // just a test url so that it doesnt return error again and again lol
}

module.exports.getQueue = function(server) {
    if (!server || server == "") {
        return false;
    } else {
        return queue[server]; 
    }
}

module.exports.getVideoId = function (url){
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
    var match = url.match(regExp);
    return (match&&match[7].length==11)? match[7] : false;
}


module.exports.onBotFinishPlaying = function(msg) {
    timer[msg.guild.id] = setTimeout(function() {
        if (queue[msg.guild.id] == undefined || queue[msg.guild.id].length == 0) {
            var voiceChannel = msg.member.voiceChannel;
            if (!voiceChannel) { return false; }
            voiceChannel.leave();
            voiceChannel = msg.member.voiceChannel;
            channel[msg.guild.id] = undefined;
            queue[msg.guild.id] = [];
            console.log("Left #" + voiceChannel.name + " due to inactive music player");
        } else {
            if (timer[msg.guild.id]) {
                clearTimeout(timer[msg.guild.id]);
                console.log("Timeout Cleared.");
            } else {
                console.log("There's no timer!");
                return false
            }
        }
    },  30000);
}

module.exports.stream = function(type, msg, url) {
    const streamOptions = { seek: 0, volume: 1 };
    var voiceChannel = msg.member.voiceChannel;
    if (!voiceChannel) { 
        msg.reply("You need to be in voice channel before using this command"); 
        return false;
    }  else {
        voiceChannel.join().then(connection => {
            conn[msg.guild.id] = connection;
            const stream = ytdl(url, {filter: 'audioonly', quality: 'highestaudio', highWaterMark: 1024 * 1024 * 10});
            stream.on("error", console.error)
            const dispatcher = conn[msg.guild.id].playStream(stream);
            if (type == "skip") {
                if (this.getNextSongAfterSkip(msg.guild.id) == false) {
                    msg.channel.send("**Music**: Playback Stopped. There are no more song streams in queue list.")
                    queue[msg.guild.id] = []
                    return dispatcher.end();
                } else {
                    msg.channel.send("**Music**: Skipped Current Track.");
                    return dispatcher.end();
                }
            } else if (type == "pause") {
                return dispatcher.pause();
            } else if (type == "resume") {
                return dispatcher.resume();
            } 
            dispatcher.on("end", end => {
                let next = this.nextSong(msg.guild.id);
                var l = queue[msg.guild.id].length
                if (next == false || next == undefined || !next || l == 0) {
                    //msg.channel.send(":notes:  **Queue:** There is nothing left to play from queue. Use " + config.get('bot.prefix') + "leave to leave the voice channel.");
                    this.onBotFinishPlaying(msg);
                    return true;
                } else {
                    youtube(this.getVideoId(next), function (err, videoInfo) {
                        if (err) return console.log("Oops! Something went wrong! [ERROR CODE: 3]");

                        msg.channel.send(":notes:  **Now Playing:** ``" + videoInfo.title + "`` **Uploaded by**: ``" + videoInfo.owner + "`` **Duration: ** ``" + toTime(videoInfo.duration) + "``");
                      });
                    return this.stream("nil", msg, next);
                }                    
            });
        }).catch(err => console.log(err));   
    }
    return true;
}
