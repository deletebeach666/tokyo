/*
    @official music library of SchwiftyBot
    @author Om Bhende
    @date 8 April 2020 (I hope corona goes away)
    @project SchwiftyBot
*/
var ANTI_SPAM = 3 // seconds for each music command.
var ANTI_SPAM_SERVER = [];

module.exports.spamCheck = function(server, timestamp) {
    var unix = Math.round(+new Date()/1000);
    if (ANTI_SPAM_SERVER[server] == undefined || !ANTI_SPAM_SERVER[server]) ANTI_SPAM_SERVER[server] = 0;
    var diff = timestamp - ANTI_SPAM_SERVER[server]
    if (diff < ANTI_SPAM) {
        return [false, diff, ANTI_SPAM]
    } else {
        ANTI_SPAM_SERVER[server] = unix;
        return [true, diff, ANTI_SPAM]
    }
}