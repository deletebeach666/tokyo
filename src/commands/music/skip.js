const Commando = require("discord.js-commando");
const request = require("request");
const config = require('../../Config.js');
const schwifty = require('../../functions.js');
const music = require('../../music.js');

module.exports = class MusicSkipCommand extends Commando.Command {
    constructor(client) {
        super (client, {
            name: "skip",
            group: "music",
            memberName: "skip",
            description: "Skips track from queue.",
            examples: ["skip"],
        });
    }
    
    async run(msg, args, client) {
        // Spam Check
        var currentTimestamp = Math.round(+new Date()/1000);
        var spam = schwifty.spamCheck(msg.guild.id, currentTimestamp);
        if (!spam[0] || spam[0] == undefined) return msg.reply("You can only use music commands once in " + spam[2] + " seconds!");
        var voiceChannel = msg.member.voiceChannel;
        if (!voiceChannel || voiceChannel == undefined) { return msg.reply("You need to be in voice channel to use this command"); }
        music.skipTrack(msg, voiceChannel);
    }
};