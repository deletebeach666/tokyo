const Commando = require("discord.js-commando");
const request = require("request");
const config = require('../../Config.js');
const music = require('../../music.js');
const schwifty = require('../../functions.js');
const yts = require( 'yt-search' )

module.exports = class MusicPlayCommand extends Commando.Command {
    constructor(client) {
        super (client, {
            name: "play",
            aliases: ['yt', 'p', 'music'],
            group: "music",
            memberName: "play",
            description: "Music Player.",
            examples: ["play <yt url>"],
            
            args: [
                {
                    key: 'songName',
                    label: 'songName',
                    prompt: 'Which Music would you like to play?',
                    type: 'string'
                }
            ]
        });
    }
    
    async run(msg, args, client) {  
        // Spam Check
        var currentTimestamp = Math.round(+new Date()/1000);
        var spam = schwifty.spamCheck(msg.guild.id, currentTimestamp);
        if (!spam[0] || spam[0] == undefined) return msg.reply("You can only use music commands once in " + spam[2] + " seconds!");
        
        // Music Info.
        var isLink = args.songName;
        var voiceChannel = msg.member.voiceChannel;
        if (!voiceChannel || voiceChannel == undefined) { return msg.reply("You need to be in voice channel to use this command"); }
        if (music.getVideoId(isLink)) {
            music.addQueue(msg.guild, isLink, msg, voiceChannel); 
        } else {
            msg.channel.send(":mag_right:  Finding ``" + args.songName + "`` on :tv:Youtube.")
            .then (message => {
                message.delete(3000);
            });
            yts(args.songName, function (err, r) {
                if (r == undefined) return false;
                if (!r.videos || r.videos == undefined) return msg.reply("Oops! Something went wrong! Try again later. [ERROR CODE: 4]");
                const videos = r.videos
                if (!videos || videos == undefined) { return msg.reply("Sorry, I could not find any song on youtube with requested keyword!")}
                music.addQueue(msg.guild, videos[0].url, msg, voiceChannel); 
                searchmsg.delete()
                    .then(msg => console.log(`Deleted message from ${msg.author.username}`))
                    .catch(console.error); 
            });
        }
            
    }
};