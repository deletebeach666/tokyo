const Commando = require("discord.js-commando");
const request = require("request");
const config = require('../../Config.js');
const schwifty = require('../../functions.js');
const music = require('../../music.js');

module.exports = class MusicLeaveCommand extends Commando.Command {
    constructor(client) {
        super (client, {
            name: "leave",
            group: "music",
            memberName: "leave",
            description: "Leaves Voice Channel and clears queue.",
            examples: ["leave"],
        });
    }
    
    async run(msg, args, client) {
        // Spam Check
        var currentTimestamp = Math.round(+new Date()/1000);
        var spam = schwifty.spamCheck(msg.guild.id, currentTimestamp);
        if (!spam[0] || spam[0] == undefined) return msg.reply("You can only use music commands once in " + spam[2] + " seconds!");
        
        var leave = music.leaveChannel(msg);
    }
};