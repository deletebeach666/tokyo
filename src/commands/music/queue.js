const Commando = require("discord.js-commando");
const request = require("request");
const config = require('../../Config.js');
const music = require('../../music.js');
const schwifty = require('../../functions.js');
const youtube = require("youtube-info");

module.exports = class MusicQueueCommand extends Commando.Command {
    constructor(client) {
        super (client, {
            name: "queue",
            group: "music",
            memberName: "queue",
            description: "Music Player Queue",
            examples: ["queue"],
        });
    }
    
    async run(msg, args, client) {
        // Spam Check
        var currentTimestamp = Math.round(+new Date()/1000);
        var spam =schwifty.spamCheck(msg.guild.id, currentTimestamp);
        if (!spam[0] || spam[0] == undefined) return msg.reply("You can only use music commands once in " + spam[2] + " seconds!");

        var queue = music.getQueue(msg.guild.id);
        var q = "**Queue:**\n";
        if (queue == undefined || queue.length == undefined || queue.length == 0) { return msg.reply("Server queue is currently empty."); }
        for (var i=0;i < queue.length; i++) {
            if (i == 0) {
                q += "[Now Playing] #" + i + ": ``" + queue[i][1] + "``\n";
            } else {
                q += "#" + i + ": ``" + queue[i][1] + "``\n";
            }
        }
        msg.channel.send(q);
    }
};